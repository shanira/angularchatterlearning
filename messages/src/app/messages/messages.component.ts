import { MessagesService } from './messages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  
  //messages = ['Message 1','Message 2' ,'Message 3'];
  messages;
  messagesKeys;

  constructor(private service:MessagesService) {
   // let service = new MessagesService();
    service.getMessages().subscribe(
      response=>{
        //console.log(response.json());
        this.messages = response.json();
        this.messagesKeys = Object.keys(this.messages);
    }); //הרשמה ל observable
  }

  ngOnInit() {
  }
}