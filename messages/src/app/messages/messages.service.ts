import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
@Injectable()
export class MessagesService {

  //שמאל תכונה, ימין סוג המחלקה
  http:Http; //המחלקה מנהלת את התקשורת עם השרת, ספציפי עם הרסט איפיאי

  getMessages(){
   // return ['Message 1','Message 2' ,'Message 3' ,'Message 4'];
   //get messages from the SLIM rest API (No DB)

   return this.http.get('http://localhost/angular/slim/messages');
  }
  postMessage(data){
   //המרת גייסון למפתח וערך
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }

    let params = new HttpParams().append('message',data.message);
    
    return this.http.post('http://localhost/angular/slim/messages', params.toString(), options);

  }

  //בתוך הקונסטרקטור נוצר מופע של האובייקט, דפנדיסי אינג'קטיון
  constructor(http:Http) { 
    this.http = http;
  }

}