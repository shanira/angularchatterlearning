import { HttpModule } from '@angular/http';//
import { MessagesService } from './messages/messages.service';//
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesFormsComponent } from './messages-forms/messages-forms.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    MessagesFormsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,//
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: [
    MessagesService //
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
