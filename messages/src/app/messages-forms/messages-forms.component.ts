import { MessagesService } from '../messages/messages.service';
import { Component, OnInit } from '@angular/core';
import {FormGroup , FormControl} from '@angular/forms';

@Component({
  selector: 'messagesForms',
  templateUrl: './messages-forms.component.html',
  styleUrls: ['./messages-forms.component.css']
})
export class MessagesFormsComponent implements OnInit {
  //בניית מבנה נתונים בקוד שהוא מתאים אחד לאחד בטופס

  service:MessagesService;

  //Reactive Form
  msgform = new FormGroup({
    message:new FormControl(),
    user:new FormControl()
  });

  //שליחת הנתונים
  sendData() {
    console.log(this.msgform.value);
    this.service.postMessage(this.msgform.value).subscribe(
      response => {
        console.log(response.json())
      }
    );
  }

  constructor(service: MessagesService) { 
    this.service = service;
  }

  ngOnInit() {
  }

}